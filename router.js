const express = require('express')
const router = express.Router()
const { createFile,getFiles, getFile, putFile, deleteFile } = require('./fsSystem')

router.get('/api/files', getFiles);
router.post('/api/files', createFile);
router.get('/api/files/:filename', getFile);
router.put('/api/files/:filename', putFile);
router.delete('/api/files/:filename', deleteFile);

module.exports = router;